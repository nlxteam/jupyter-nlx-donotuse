#!/usr/bin/env sh

# make sure we are in the correct folder
cd /home/jovyan

# generate a new token
RND_TOKEN=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)
JUPYTER_TOKEN=${JUPYTER_TOKEN:-${RND_TOKEN}}
if [ -d "/data" ]; then
  echo "${JUPYTER_TOKEN}" > /data/token 
fi
echo "Token: ${JUPYTER_TOKEN}"

# start jupyter notebook
jupyter notebook --allow-root --NotebookApp.token="${JUPYTER_TOKEN}"
