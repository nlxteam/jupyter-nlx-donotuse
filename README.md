# jupyter-nlx

Docker image based on jupyter/scipy-notebook with added support for java kernels.

## Run example:

docker run -p 8888:8888 nlxsoftware/jupyter-nlx
